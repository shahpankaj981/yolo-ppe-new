## Getting Started

1. Clone the git repository

   ```
   git clone https://gitlab.com/shahpankaj981/yolo-ppe-new
   ```

   


2. Run

   ```
   python3 video_demo.py --video_file_name="[FILE_NAME]" --camera_id=[None]
   ```

   where,

   `[FILE_NAME]` = name of video file (OPTIONAL, default is video file named "input.mp4")

   `[CAMERA_ID]` = Boolean (OPTIONAL, if not given, tries to work on video)
   

## Link to Dataset in YOLO format 

   ```
   https://drive.google.com/file/d/1nLAY4qakgXt_Moz5zN8C6sg9WU8DuNYx/view?usp=sharing
   ```
   